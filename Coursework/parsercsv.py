from bs4 import BeautifulSoup
import requests
import csv


URL = "https://opusdeco.ru/catalog/wallpapers/york-wallcoverings/?PAGE_CNT=201"
HEADERS = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.61 Safari/537.36'
    }

response = requests.get(URL, headers = HEADERS)
soup = BeautifulSoup(response.content, 'html.parser')
items = soup.findAll('div',class_ = 'bx_catalog_item_container')

for item in items:
    title = item.find('div', class_='bx_catalog_item_title').get_text(strip = True)
    price =   item.find('div',class_='bx_price').get_text(strip = True)
    link =item.find('a').get('href')

    with open('site.csv', mode="a", encoding='utf-8') as file:
        file_writer = csv.writer(file, delimiter = ",", lineterminator="\r")
        file_writer.writerow([title, price, link])
