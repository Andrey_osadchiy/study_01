import math 
# Две пустые строки между импортами и кодом

a = input()
length = len(a)
# Функцию ceil можно и не использовать, есть математические операторы, которые делают тоже самое
b = math.ceil(length/2)
# Перед скобкой пробел не ставится
# При взятии среза, если хотите от начала списка идти, то необязательно указывать 0
print(a[b::] + a[0:b] )
