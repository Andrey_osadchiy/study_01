input()
array = list(map(int, input().strip().split(" ")))
count = 0

for i in range(1, len(array)):
    for j in range(len(array) - i):
        if array[j] > array[j + 1]:
            array[j], array[j + 1] = array[j + 1], array[j]
            count += 1
print(count)