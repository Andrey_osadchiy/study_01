import datetime

import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func


Base = declarative_base()


class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)

# таблица с магазинами в тц
class Stores(CommonMixture, Base):
    __tablename__ = "stores"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)
    isworking = sa.Column(sa.Boolean, nullable=False)

class Product(CommonMixture, Base):
    __tablename__ = "products"

    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)
    # prices = relationship("ProductPrice", back_populates="product", uselist=True)


class Trade(CommonMixture, Base):
    __tablename__ = "trades"

    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    # product = relationship("Product", back_populates="prices", uselist=False)
    amount = sa.Column(sa.Float, nullable=False)


def add(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.add(record)
        self._session.commit()
        return record
    return wrapper


def delete(function):
    def wrapper(self, *args, **kwargs):
        record = function(self, *args, **kwargs)
        self._session.delete(record)
        self._session.commit()
    return wrapper


class Role:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class StoreSellerRole(Role):
    def get_product(self, name):
        return self._session.query(Product).filter(Product.name == name).first()

    def get_amount_product(self, product):
        return self._session.query(func.sum(Trade.amount).label("amount")).filter(
            Trade.product_id == product.id,
        ).first()[0]

    @add
    def sale(self, name, amount):
        product = self.get_product(name)
        if product is None:
            raise Exception("Has no product with name '%s'" % name)
        
        has_amount = self.get_amount_product(product)
        if amount > has_amount:
            raise Exception("Has no %f amount for sell '%s'. Current amount: %f" % (
                amount, name, has_amount,
            ))

        return Trade(product_id=product.id, amount=-amount)


class StoreAdminRole(StoreSellerRole):
    @add
    def income(self, name, amount):
        product = self.get_product(name)
        if product is None:
            product = Product(name=name)
            self._session.add(product)
            self._session.commit()

        return Trade(product_id=product.id, amount=amount)

class TcAdminRole(Role):
    def get_store(self, name):
        return self._session.query(Stores).filter(Stores.name == name).first()

    def add_shops(self, name, isworking):
        stores = self.get_store(name)
        if stores is None:
            stores = Stores(name=name, isworking=isworking)
            self._session.add(stores)
            self._session.commit()


    def close_shops(self, name):
        stores = self.get_store(name)
        if stores is None:
            raise Exception("Has no store in shopping mall with name '%s'" % name)
        else:
            #stores = Stores(name=name) из-за этой строчки всё и не работало facepalm
            self._session.delete (stores)
            self._session.commit()

admin = TcAdminRole(engine="sqlite", name="store102.db")
admin.add_shops(name="Bukvoed", isworking=True)
admin.add_shops(name="Formula Kino", isworking=True)
admin.add_shops(name="MCdonalds", isworking=True)
admin.add_shops(name="ZARA", isworking=True)
admin.add_shops(name="HM", isworking=True)
admin.close_shops(name="ZARA") 
storeadmin = StoreAdminRole(engine="sqlite", name="store102.db")
seller =StoreSellerRole(engine="sqlite", name="store102.db")
storeadmin.income(name="Книга", amount=10)
seller.sale(name="Книга", amount=5)
