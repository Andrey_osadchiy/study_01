import os
import csv
import xml.etree.ElementTree as ET


directory = 'D:/andrey/pythoon_scripts/xml_parser'
files = os.listdir(directory)
#print (files)
for i in files:
    name_xml = i
    tree = ET.parse(i)
    root = tree.getroot()
    for form in root.findall(".*/*/{http://zakupki.gov.ru/oos/types/1}fullName"):
        fullname = form.text

    for form in root.findall(".*/*/{http://zakupki.gov.ru/oos/types/1}inn"):
        inn = form.text

    for form in root.findall(".*/*/*/{http://zakupki.gov.ru/oos/types/1}budget"):
        budget = form.text

    for form in root.findall(".*/{http://zakupki.gov.ru/oos/types/1}contractSubject"):
        contractsubject = form.text

    for form in root.findall(".*/{http://zakupki.gov.ru/oos/types/1}signDate"):
        signDate = form.text

    for form in root.findall(".*/{http://zakupki.gov.ru/oos/types/1}regNum"):
        regNum = form.text

    for form in root.findall(".*/*/{http://zakupki.gov.ru/oos/types/1}price"):
        price = form.text

    for form in root.findall(".*/*/*/*{http://zakupki.gov.ru/oos/types/1}firmName"):
       firmName = form.text
    
    with open("contracts.csv", mode="a", encoding='utf-8') as w_file:
        file_writer = csv.writer(w_file, delimiter = ",", lineterminator="\r")
        file_writer.writerow([i,inn,fullname, signDate, regNum, price,contractsubject])
