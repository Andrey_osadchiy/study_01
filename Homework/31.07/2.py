input()
array1 = list(map(int, input().strip().split(" ")))
array2 = list(map(int, input().strip().split(" ")))

for element in array2:
    left = 0
    right = len(array1) - 1
    while left <= right:
        med = (left + right) // 2
        if array1[med] < element:
            left = med + 1
        elif array1[med] > element:
            right = med - 1
        else:
            print(array1[med])
            break
    else:

        if right >= 0:
            if left < len(array1):
                if element - array1[right] > array1[left] - element:
                    print(array1[left])
                else:
                    print(array1[right])
            else:
                print(array1[right])
        else:
            print(array1[left])
