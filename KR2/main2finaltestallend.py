# Не принято - чуть чуть доделать нужно

# Торгово-развлекательный центр. Электронном хранилище документации
import datetime
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker
from sqlalchemy.sql import func

Base = declarative_base()


##ниже класс чтобы не надо было в каждый класс писать id и время создания
class CommonMixture:
    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    created_at = sa.Column(sa.DateTime, nullable=False, default=datetime.datetime.now)


# таблица с магазинами в тц
class Stores(CommonMixture, Base):
    __tablename__ = "stores"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)
    # Почему именно BOOLEAN вместо Boolean? Boolean более гибок для разных БД
    isworking = sa.Column(sa.BOOLEAN, nullable=False)


# таблица с продуктами в тц
class Product(CommonMixture, Base):
    __tablename__ = "product"
    name = sa.Column(sa.Unicode(64), unique=True, nullable=False)


# store_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Stores.__tablename__}.id"), nullable=False)
# таблица со сделками
class Trade(CommonMixture, Base):
    __tablename__ = "trades"
    product_id = sa.Column(sa.Integer, sa.ForeignKey(f"{Product.__tablename__}.id"), nullable=False)
    amount = sa.Column(sa.Float, nullable=False)


class Role:
    def __init__(self, engine, name, host=None, port=None, username=None, password=None, **params):
        dsn = f"{engine}://"
        if username is not None:
            dsn += username
        if password is not None:
            dsn += f":{password}"
        if username is not None:
            dsn += "@"
        if host is not None:
            dsn += f"{host}"
        if port is not None:
            dsn += f":{port}"
        dsn += f"/{name}"
        if params:
            dsn += "?"
        dsn += "&".join(f"{key}={value}" for key, value in params.items())

        self._engine = sa.create_engine(dsn)
        Base.metadata.create_all(self._engine)
        self._session = sessionmaker(bind=self._engine)()


class SellerRole(Role):
    def get_product(self, name):
        return self._session.query(Product).filter(Product.name == name).first()

    def get_amount_product(self, product):
        return self._session.query(func.sum(Trade.amount).label("amount")).filter(
            Trade.product_id == product.id,
        ).first()[0]

    def sale(self, name, amount):
        product = self.get_product(name)
        if product is None:
            raise Exception("Has no product with name '%s'" % name)

        has_amount = self.get_amount_product(product)

        if amount > has_amount and type(has_amount) is not NoneType:
            raise Exception("Has no %f amount for sell '%s'. Current amount: %f" % (
                amount, name, has_amount,
            ))

        return Trade(product_id=product.id, amount=-amount)

    def income(self, name, amount):
        product = self.get_product(name)
        if product is None:
            product = Product(name=name)
            self._session.add(product)
            self._session.commit()

        return Trade(product_id=product.id, amount=amount)


class AdminRole(Role):
    def get_store(self, name):
        return self._session.query(Stores).filter(Stores.name == name).first()

    def add_shops(self, name, isworking):
        stores = self.get_store(name)
        if stores is None:
            stores = Stores(name=name, isworking=isworking)
            self._session.add(stores)
            self._session.commit()

    # 1 def del_shops(self, name):
    #    stores.update().values(isworking=False).where(
    #    stores.c.name==select([stores.c.name])
    #    )
    # 2 def close_shops(self, name, isworking):
    #  stores = self.get_store(name)
    #   if stores is None:
    #        raise Exception("Has no store with name '%s'" % name)
    #   return Stores(name=name, isworking=isworking)

    # 3 def close_shops(self, name, isworking):
    #    query = update(Stores).where(Stores.name == name).values(Table.isworking == isworking)
    # 4 def close_shops(self, name):
    #    stores = self.get_store(name)
    #    if stores is not None:
    #        stores = Stores(name=name)
    #        self._session.delete (stores)
    #        self._session.commit()


admin = AdminRole(engine="sqlite", name="sed4.db")
admin.add_shops(name="Bukvoed", isworking=True)
admin.add_shops(name="Formula Kino", isworking=True)
admin.add_shops(name="MCdonalds", isworking=True)
admin.add_shops(name="ZARA", isworking=True)
# admin.close_shops(name="ZARA") # я сдаюсь -  я не понимаю почему не работает  ни она из функций что описал выше по удалению магазинов
# Ну вот 4-я олжна работать, только мне непонятно, что Вы делаете, если не находите магазина - создаёте новый и удаляете его?
# Отсюда и ошибка, которая описана ниже в Вашем комментарии
# Instance '<Stores at 0x3d81328>' is not persisted


seller = SellerRole(engine="sqlite", name="sed4.db")
seller.income(name="Книга", amount=100)
seller.income(name="Гамбургер", amount=50)
seller.income(name="Шорты", amount=10)
seller.income(name="Блокбастер", amount=3)
#seller.sale(name="Шорты", amount=2)  # я не понимаю ошибку - not supported between instances of 'int' and 'NoneType'
# has_amount у Вас возвращается None в методе sale
# Вы пытаетесь сравнивать amount (int) и has_amount (NoneType)
