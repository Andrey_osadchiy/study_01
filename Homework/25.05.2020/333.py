# Принято

a = int(input())
b = int(input())
s = ''

# Можно оптимизировать. a=0, b=10**6
# В Вашем случае это миллион итераций. А можно сократить до полумиллиона
while a <= b:
    if a % 2 == 0:
        s += str(a)
    a += 1
print(s)
